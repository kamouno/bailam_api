import env

import unittest
import bailamapi
import pandas as pd
import time
import os
from datetime import date

class TestExistingLink(unittest.TestCase):

    def test_create_smartlink(self):
        ba = bailamapi.BailamAPI()
        pkid = ba.create_systemlink_from_files("test_from_python", env.TEST_DATA_FOLDER+"FIXsource.xlsx", env.TEST_DATA_FOLDER+"FIXtarget.xlsx")
        self.assertGreater(pkid, 0)

        ba = bailamapi.BailamAPI()
        with self.assertRaises(ValueError):
            f = ba.def_mapping_function(-1)
        f = ba.def_mapping_function(pkid)
        a = pd.read_excel(env.TEST_DATA_FOLDER+"FIXsource.xlsx")
        b = f(a)
        self.assertEqual(len(a), len(b))


        file = env.TEST_DATA_FOLDER+"FIXsource.xlsx"
        a = pd.read_excel(file)
        b = ba.map_new_file(file, pkid)
        self.assertEqual(len(a), len(b))

        bdf = bailamapi.BailamAPI()
        pkid = bdf.create_systemlink("test_from_python", a ,env.TEST_DATA_FOLDER + "FIXtarget.xlsx")
        time.sleep(4)
        b = bdf.map_new_file(file)
        self.assertEqual(len(a), len(b))


    def test_create_smalDF(self):
        ba = bailamapi.BailamAPI()
        dfa=pd.DataFrame([{'id':'a1','x':2,'y':3,'z':'US'},{'id':'a2','x':5,'y':6,'z':'FR'},{'id':'a3','x':5,'y':6,'z':'IL'}])
        dfb = pd.DataFrame([{'id': 'a1', 'xpy': 5, 'oldy': 3, 'z': 'USA'}, {'id': 'a2', 'xpy': 11, 'oldy': 6, 'z': 'FRA'}])

        pkid = ba.create_systemlink("test_from_python", dfa, dfb)
        self.assertGreater(pkid, 0)

        ba = bailamapi.BailamAPI()
        with self.assertRaises(ValueError):
            f = ba.def_mapping_function(-1)
        f = ba.def_mapping_function(pkid)
        fc= ba.get_mapping_function_code()
        b = f(dfa)
        self.assertEqual(len(dfa), len(b))



        b = ba.map(dfa)
        self.assertEqual(len(dfa), len(b))

        f = ba.def_mapping_function()


    def test_create_init(self):

        dfa=pd.DataFrame([{'id':'a1','x':2,'y':3,'z':'US'},{'id':'a2','x':5,'y':6,'z':'FR'},{'id':'a3','x':5,'y':6,'z':'IL'}])
        dfb = pd.DataFrame([{'id': 'a1', 'xpy': 5, 'oldy': 3, 'z': 'USA'}, {'id': 'a2', 'xpy': 11, 'oldy': 6, 'z': 'FRA'}])
        ba = bailamapi.BailamAPI("test_from_python", dfa, dfb)

        f = ba.def_mapping_function()
        fc= ba.get_mapping_function_code()
        b = f(dfa)
        self.assertEqual(len(dfa), len(b))



        b = ba.map(dfa)
        self.assertEqual(len(dfa), len(b))

        f = ba.def_mapping_function()



class TestfromJson(unittest.TestCase):

    def test_create_smartlink(self):
        source_json = {'clients': [
            {'country': 'US', 'users': [{'id': 'a1', 'age': 2, 'name': 'david', 'citizen': 'US', 'year': 2021},
                                        {'id': 'a11', 'age': 5, 'name': 'olivier', 'citizen': 'CH', 'year': 2021}]},
            {'country': 'FR', 'users': [{'id': 'a2', 'age': 5, 'name': 'johN', 'citizen': 'FR', 'year': 2021},
                                        {'id': 'a3', 'age': 10, 'name': 'Jhony', 'citizen': 'IL', 'year': 2021}]},
            {'country': 'IL', 'users': [{'id': 'a4', 'age': 20, 'name': 'Noa', 'citizen': 'CH', 'year': 2021}]},
            {'country': 'CH', 'users': [{'id': 'a5', 'age': 11, 'name': 'Yousouf', 'citizen': 'CH', 'year': 2021}]}
            ]}

        target_df = pd.DataFrame(
            [{'id': 'a1', 'nom': 'DAVID', 'native': 'YES', 'cty': 'USA', 'birth_year': 2019, 'oldy': 4},
             {'id': 'a2', 'native': 'YES', 'nom': 'JOHN', 'oldy': 10, 'cty': 'FRA', 'birth_year': 2016},
             {'id': 'a3', 'native': 'NO', 'nom': 'JOHNY', 'oldy': 20, 'cty': 'FRA', 'birth_year': 2011}])

        ba = bailamapi.BailamAPI("auto_CD_test"+str(date.today()),source=source_json,target=target_df)

        res = ba.map(source_json)
        self.assertEqual(len(res), 6)

        nb_tmp = os.listdir(ba.tmp_folder)



class TestfromXML(unittest.TestCase):

    def test_create_smartlink(self):
        source_json = {'clients': [
            {'country': 'US', 'users': [{'id': 'a1', 'age': 2, 'name': 'david', 'citizen': 'US', 'year': 2021},
                                        {'id': 'a11', 'age': 5, 'name': 'olivier', 'citizen': 'CH', 'year': 2021}]},
            {'country': 'FR', 'users': [{'id': 'a2', 'age': 5, 'name': 'johN', 'citizen': 'FR', 'year': 2021},
                                        {'id': 'a3', 'age': 10, 'name': 'Jhony', 'citizen': 'IL', 'year': 2021}]},
            {'country': 'IL', 'users': [{'id': 'a4', 'age': 20, 'name': 'Noa', 'citizen': 'CH', 'year': 2021}]},
            {'country': 'CH', 'users': [{'id': 'a5', 'age': 11, 'name': 'Yousouf', 'citizen': 'CH', 'year': 2021}]}
            ]}

        target_df = pd.DataFrame(
            [{'id': 'a1', 'nom': 'DAVID', 'native': 'YES', 'cty': 'USA', 'birth_year': 2019, 'oldy': 4},
             {'id': 'a2', 'native': 'YES', 'nom': 'JOHN', 'oldy': 10, 'cty': 'FRA', 'birth_year': 2016},
             {'id': 'a3', 'native': 'NO', 'nom': 'JOHNY', 'oldy': 20, 'cty': 'FRA', 'birth_year': 2011}])

        ba = bailamapi.BailamAPI("auto_CD_test"+str(date.today()),source=source_json,target=target_df)

        res = ba.map(source_json)
        self.assertEqual(len(res), 6)

        nb_tmp = os.listdir(ba.tmp_folder)

    def test_create_smartlink_with_leaning_env(self):
        dfa = pd.DataFrame([{'id': 'a1', 'x': 2, 'y': 3, 'z': 'US'}, {'id': 'a2', 'x': 5, 'y': 6, 'z': 'FR'},
                            {'id': 'a3', 'x': 5, 'y': 6, 'z': 'IL'}])
        dfb = pd.DataFrame(
            [{'id': 'a1', 'xpy': 5, 'oldy': 3, 'z': 'USA'}, {'id': 'a2', 'xpy': 11, 'oldy': 6, 'z': 'FRA'}])


        ba = bailamapi.BailamAPI("auto_CD_test"+str(date.today()),source=dfa,target=dfb,
                                 learning_env={"YLP_ADD_DUMMY_TO_LEARN":True})

        res = ba.map(dfa)
        self.assertEqual(len(res), 3)

        nb_tmp = os.listdir(ba.tmp_folder)


if __name__ == '__main__':
    unittest.main()
