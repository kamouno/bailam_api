import sys
import os

# append module root directory to sys.path
sys.path.append(
    os.path.dirname(
        os.path.dirname(
            os.path.abspath(__file__)
        )
    )
)

TEST_DATA_FOLDER = os.path.dirname(os.path.abspath(__file__)) + '/data/'


