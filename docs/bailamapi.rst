.. _bailamapi:


***************
BailamAPI
***************

.. _bailamapi-intro:

BailamAPI introduction
=============================

BailamAPi module let use leverage the learning by example engine of Bailam within any python or jupyter project

A Systemlink is the result of Bailam learning process that contain teh information about the link generated to transform
the data frm a source to a target.

The simplest way to create a smartlink from a source data frame df_source to a target df_target is ::



    import bailamapi
    ba = bailamapi.BailamAPI("My first System link " ,df_source,df_target)


or with named parameter ::

    ba = bailamapi.BailamAPI(link_name= "My first System link " ,source= df_source,target = df_target)


you can  find an jupyter example at : https://colab.research.google.com/drive/1s_ZrBOgPZvZdj5_ieFp8VUm5oeiqBFe4?usp=sharing


BailamAPI class
=============================

BailamAPI class manage the connection to Bailan ReST server and the exchange of data


.. automodule:: bailamapi.BailamAPI
    :members:
